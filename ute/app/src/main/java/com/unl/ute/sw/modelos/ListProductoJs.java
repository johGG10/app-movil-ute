package com.unl.ute.sw.modelos;

import java.util.ArrayList;
import java.util.List;

public class ListProductoJs {

    private String msg;
    private Integer code;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public List<ProductoJs> getData() {
        return data;
    }

    public void setData(List<ProductoJs> data) {
        this.data = data;
    }

    private List<ProductoJs> data = new ArrayList<>();


}
