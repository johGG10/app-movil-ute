package com.unl.ute.sw;

import android.content.Context;

import androidx.annotation.NonNull;

import com.android.volley.Request;
import com.android.volley.Response;
import com.unl.ute.sw.modelos.ListPersonaJS;
import com.unl.ute.sw.modelos.ListProductoJs;

import java.util.List;

public class Peticiones {

    public static String API_URL = "http://192.168.1.4:4000/api/v1/";

    public static VolleyRequest<ListProductoJs> getListaProductos(
            @NonNull final Context context,
            @NonNull Response.Listener<ListProductoJs> response,
            @NonNull Response.ErrorListener errorListener
            ){
        final String url = API_URL + "producto";
        VolleyRequest request = new VolleyRequest(
                context,
                Request.Method.GET,
                url,
                response, errorListener
        );
        request.setResponseClass(ListProductoJs.class);
        return request;
    }

    public static VolleyRequest<ListPersonaJS> getListaPersonas(
            @NonNull final Context context,
            @NonNull Response.Listener<ListPersonaJS> response,
            @NonNull Response.ErrorListener errorListener
    ){
        String url = API_URL + "persona";
        VolleyRequest request = new VolleyRequest(
                context,
                Request.Method.GET,
                url,
                response, errorListener
        );
        request.setResponseClass(ListPersonaJS.class);
        return request;
    }
}
