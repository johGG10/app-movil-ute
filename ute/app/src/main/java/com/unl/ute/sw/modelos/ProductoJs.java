package com.unl.ute.sw.modelos;
//    {
//        "nombre": "Chocolate blanco",
//         "p_compra": 1.25,
//            "p_venta": 2,
//            "estado": true,
//            "external_id": "edb5627a-ffab-475f-be2f-6425d7ada2d2",
//            "id_categoria": 1
//    },

public class ProductoJs {
    private String nombre;
    private String p_compra;
    private String p_venta;
    private String estado;
    private String external_id;
    private String id_categoria;

    public String getNombre() {
        return nombre;
    }

    public String getP_compra() {
        return p_compra;
    }

    public String getP_venta() {
        return p_venta;
    }

    public String getEstado() {
        return estado;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setP_compra(String p_compra) {
        this.p_compra = p_compra;
    }

    public void setP_venta(String p_venta) {
        this.p_venta = p_venta;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public void setId_categoria(String id_categoria) {
        this.id_categoria = id_categoria;
    }

    public String getExternal_id() {
        return external_id;
    }

    public String getId_categoria() {
        return id_categoria;
    }
}
