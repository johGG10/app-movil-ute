package com.unl.ute.adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.unl.ute.R;

import com.unl.ute.sw.modelos.ProductoJs;

import java.util.List;

public class ListaAdaptadorProducto extends ArrayAdapter<ProductoJs> {
    private Context context;
    private List<ProductoJs> lista;

    public ListaAdaptadorProducto( Context context,  List<ProductoJs> lista) {
        super(context, R.layout.lista_item,lista);
        this.context = context;
        this.lista=lista;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View item = inflater.inflate(R.layout.lista_item, null);

        final ProductoJs podProductoJS = lista.get(position);
        TextView txtnombre = item.findViewById(R.id.nombre);
        TextView precio = item.findViewById(R.id.precio);
        txtnombre.setText(podProductoJS.getNombre());
        precio.setText(podProductoJS.getP_venta().toString());
        return item;
    }

}
